module GoBackend

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
